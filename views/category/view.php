<p>
    <?= $category->description; ?>
</p>
<div class="list-group">
    <?php foreach ($category->posts as $post): ?>
    <a href="/post/<?= $post['post_id']?>" class="list-group-item">
            <?= $post['title']?>
    </a>
    <?php endforeach; ?>
</div>