<div class="list-group">
    <div class="list-group-item-active">Список категорій</div>
    <?php foreach($categories as $category): ?>
    <a href="/category/<?= $category["category_id"]; ?>" class="list-group-item"><?= $category["name"]; ?></a>
    <?php endforeach; ?>
</div>
