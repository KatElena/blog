<div class="list-group">
    <div class="list-group-item-active">Список публікацій</div>
    <?php foreach($posts as $post): ?>
    <a href="/post/<?= $post["post_id"]; ?>" class="list-group-item"><?= $post["title"]; ?></a>
    <?php endforeach; ?>
</div>
