<div class="card text-white bg-info">
    <div class="card-body ">
        <form method="post" action="/post/add">
            <div class="form-group">
                <label for="title">Введіть заголовок</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Input Title">
            </div>
            <div class="form-group">
                <label for="short_description">Введіть короткий опис</label>
                <textarea name="short_description" id="short_description" class="form-control" placeholder="Short Description"></textarea>
            </div>
            <div class="form-group">
                <label for="description">Введіть текст публікації</label>
                <textarea name="description" id="description" class="form-control" placeholder="Text of Publication"></textarea>
            </div>
            
            <div class="list-group">
                <div class="list-group-item active">Виберіть категорії</div>
                <?php foreach ($categories as $categoryItem): ?>
                <div class="list-group-item list-group-item-info">
                    <input type="checkbox" name="categories[]" value="<?= $categoryItem["category_id"] ?>">
                    <?= $categoryItem["name"] ?>
                </div>
                <?php endforeach; ?>
            </div>
            <input type="hidden" name="action" value="add_post">
            <input type="submit" class="btn btn-primary" value="Створити">
            
        </form>
    </div>
</div>