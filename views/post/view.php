<div class="row">
    <div class="col col-md-6 col-lg-3">
        <div class="group-item">
            <div class="list-group-item list-group-item-dark">Категорія: <?= $category->name; ?></div>
        <?php foreach($category->posts as $postItem): ?>
            <a href="/post/<?= $postItem['post_id'] ?>/<?= $category->categoryId ?>" class="list-group-item <?php if ($postItem['post_id'] == $post->postId): ?>active<?php endif; ?>">
            <?= $postItem['title']; ?>
            </a>
        <?php endforeach; ?>
        </div>
    </div>
    <div class="col col-md-6 col-lg-9">
        <h1 class="mt-4"><?= $post->title; ?></h1>
        <h3><?= $post->authorName; ?></h3>
        <p style="text-align: right;">
            <?= $post->creationDate; ?>
        </p>
        <p>
            <?= $post->description; ?>
        </p>
    </div>
</div>




