<!DOCTYPE html>
<html>
    <head>
        <title><?= $title; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        <link rel="stylesheet" href="views/css/style.css">
    </head>
    <body>
        <div class="container">
          <nav class="navbar navbar-expand-lg text-white" style="background-color: #637b8c">
              <a class="navbar-brand" href="#">
                  <img src="/images/logo.png" width="30" height="30" alt="">
              </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/posts">Публікації</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/categories">Категорії</a>
                </li>
                <?php if (isset($user) && $user->isLogin): ?>
                <li class="nav-item">
                  <a class="nav-link" href="/post/add">Додати публікацію</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/logout">Вийти</a>
                </li>
                <?php else: ?>
                <li class="nav-item">
                  <a class="nav-link" href="/login">Увійти</a>
                </li>
                <?php endif; ?>
              </ul>
                <?php if (isset($user) && $user->isLogin): ?>
                 <span class="navbar-text">
                    Hello, <?= $user->firstName ?> <?= $user->lastName ?>
                 </span>
                <?php endif; ?>
            </div>
          </nav>
        </div>
        
        <div class="container mt-4">
            <?= $content; ?>
        </div>
        
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
    </body>
</html>