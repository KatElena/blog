<div class="card">
    <div class="card-body">
        <form method="post" action="/user/submit">
          <div class="form-group">
            <label for="login">Your login</label>
            <input type="text" class="form-control" name="login" id="login" placeholder="Enter login">
          </div>
          <div class="form-group">
            <label for="password">Your Password</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password">
          </div>
          <input type="hidden" name="action" value="login">
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<div class="row">
    <div class="col">
        <a class="btn btn-primary" href="/register">Реєстрація</a>
    </div>
</div>
