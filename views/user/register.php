</div><p class="h1">Реєстрація користувача</p></div>
<div class="card">
    <div class="card-body">
<div class="container bgcont">
  <div class="row background-row">
    <div class="col-lg-9 col-md-6"><div class="top-cover">
            <form method="post" action="/user/submit">
    <div class="form-group">
    <label for="login">Your login</label>
    <input type="text" class="form-control" name="login" id="login" placeholder="Enter login">
  </div>
  <div class="form-group">
    <label for="first_name">Your First Name</label>
    <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter First Name">
  </div>    
  <div class="form-group">
    <label for="last_name">Your Last Name</label>
    <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Last Name">
  </div>
  <div class="form-group">
    <label for="email">Your Email</label>
    <input type="email" class="form-control" name="email" id="email" placeholder="Enter Email">
  </div>
  <div class="form-group">
    <label for="password">Your Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password">
  </div>
  <input type="hidden" name="action" value="register">
  <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
