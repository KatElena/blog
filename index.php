<?php

define("ROOT", __DIR__);
include(ROOT . "/config/autoload.php");
include(ROOT . "/config/config.php");

$app = new Application;
$app->run();