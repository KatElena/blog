<?php

class User extends Model
{
    public $userId;
    public $login;
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $registrationDate;
    public $image;
    public $isLogin = false;
    
    public function __construct($userId = 0) 
    {
        if ($userId > 0) {
            $user = $this->getUserById($userId);
            $this->initObjectFromArray($user);
        } else {
            $this->initObjectFromArray($_POST);
            $this->image = "default.jpg";
            $this->registrationDate = date("Y-m-d h:i:s");
        }
        
    }
    
    public function register()
    {
        $sql = "INSERT INTO users (login, password, "
                . "first_name, last_name, email, image, "
                . "registration_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([
            $this->login,
            $this->password,
            $this->firstName,
            $this->lastName,
            $this->email,
            $this->image,
            $this->registrationDate,
        ]);
        $userId =  DataBase::$connection->lastInsertId();
        return $userId;
    }


    public function getUserById($userId)
    {
        $sql = "SELECT * FROM users WHERE user_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$userId]);
        $user = $stmt->fetch();
        return $user;
    }
    
    public static function checkUser()
    {
        $login = $_POST["login"];
        $password = $_POST["password"];
        $sql = "SELECT * FROM users WHERE (login=? AND password=?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([
            $login,
            $password,
        ]);
        $user = $stmt->fetch();
        return $user;
    }
}
