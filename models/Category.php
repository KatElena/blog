<?php

class Category extends Model
{
    public $categoryId;
    public $name;
    public $description;
    public $sortOrder;
    public $status;
    public $posts;
    
    public function __construct($categoryId) 
    {
        $category = $this->getCategoryById($categoryId);
        $this->initObjectFromArray($category);
        $this->posts = Post::getPostsByCategory($categoryId);
    }
    
    public static function getAllCategories()
    {
        $sql = "SELECT * FROM categories";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute();
        $categories = $stmt->fetchAll();
        return $categories;
    }
    
    public function getCategoryById($categoryId)
    {
        $sql = "SELECT * FROM categories WHERE category_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$categoryId]);
        $category = $stmt->fetch();
        return $category;
    }
}
