<?php

class Post extends Model
{
    public $postId; //post_id
    public $title;
    public $shortDescription;
    public $description;
    public $image;
    public $authorId;
    public $firstName;
    public $lastName;
    public $authorName; //Поєднання імені та прізвища
    public $creationDate;
    public $status;
    public $onTrend;
    public $categoryIds;
    
    public function __construct($postId = 0) 
    {
        if ($postId > 0) {
            $post = $this->getPostById($postId);
            $this->initObjectFromArray($post);
            $this->authorName = $this->firstName . " " . $this->lastName;
        } else {
            $this->initObjectFromArray($_POST);
            $this->image = "default.jpg";
            $this->creationDate = date("Y-m-d h:i:s");
            $this->status = 1;
            $this->categoryIds = $_POST['categories'];
        }
    }
    
    public static function getAllPosts()
    {
        $sql = "SELECT * FROM posts";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute();
        $posts = $stmt->fetchAll();
        return $posts;
    }
    
    public static function getPostsByCategory($categoryId)
    {
        $sql = "SELECT * FROM post_to_category "
                . "LEFT JOIN posts "
                . "ON post_to_category.post_id=posts.post_id "
                . "WHERE post_to_category.category_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$categoryId]);
        $posts = $stmt->fetchAll();
        return $posts;        
    }
    
    public function getPostById($postId)
    {
        $sql = "SELECT * FROM posts "
                . "LEFT JOIN users "
                . "ON posts.author_id=users.user_id "
                . "WHERE post_id=?";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([$postId]);
        $post = $stmt->fetch();
        return $post;
    }
    
    public function create()
    {
        $sql = "INSERT INTO posts (title, short_description, "
                . "description, image, author_id, creation_date, "
                . "status) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = DataBase::$connection->prepare($sql);
        $stmt->execute([
            $this->title,
            $this->shortDescription,
            $this->description,
            $this->image,
            $this->authorId,
            $this->creationDate,
            $this->status,
        ]);
        $postId =  DataBase::$connection->lastInsertId();
        if ($postId > 0 && !empty($this->categoryIds)) {
            $sql = "INSERT INTO post_to_category (post_id, category_id) "
                    . "VALUES (?, ?)";
            $stmt = DataBase::$connection->prepare($sql);
            foreach ($this->categoryIds as $categoryId) {
                $stmt->execute([
                    $postId,
                    $categoryId,
                ]);
            }
        }
        return $postId;
    }
    
}
