<?php

class Application 
{
    private $path;
    public function run()
    {
        DataBase::connect();
        //Ініціалізація сесії
        session_start();
        $this->route();
        $this->action();
    }
    
    protected function route()
    {
        $uri = trim($_SERVER['REQUEST_URI'], "/");
        $routes = include(ROOT . "/config/routes.php");
        $path = "home/page404";
        foreach ($routes as $pattern => $route) {
            if (preg_match("~^$pattern$~", $uri)) {
                $path = preg_replace("~^$pattern$~", $route, $uri);
                break;
            }
        }
        $this->path = $path;
    }
    
    protected function action() 
    {        
        $parts = [];
        $parts = explode("/", $this->path);
        $controllerId = array_shift($parts);
        $actionId = array_shift($parts);
        
        $controllerClass = ucfirst($controllerId) . "Controller";
        $action = "action" . ucfirst($actionId);
        
        $controller = new $controllerClass($controllerId);
        $controller->$action($parts);
        
    }
 
}
