<?php

class Controller 
{
    protected $view;
    protected $title;
    protected $user;
    protected $controllerId;
    protected $categories;
    
    public function __construct($controllerId) 
    {
        $this->controllerId = $controllerId;
        //Якщо в сесії ініційований користувач,
        //то створюємо обєкт користувача та 
        //задаємо його властивість isLogin із значенням істини
        if (isset($_SESSION["user"]) && !empty($_SESSION["user"])){
            $this->user = new User($_SESSION["user"]["user_id"]);
            $this->user->isLogin = true;
        }
        //Робимо доступ до категорій глобально (на всіх сторінках)
        $this->categories = Category::getAllCategories();
    }
    
    protected function render($template, $params = [])
    {
        $this->view = new View("index");
        $this->view->setParam("title", $this->title);
        
        //Передаємо користувача в вигляд
        $this->view->setParam("user", $this->user);
        //Передаємо список категорій в вигляд головного шаблону
        $this->view->setParam("categories", $this->categories);
        
        //Обєкт для генерації вбудованого HTML
        $contentView = new View($this->controllerId . "/" . $template, $params);
        $content = $contentView->getHTML();
        $this->view->setParam("content", $content);
        
        $this->view->render();
    }
}

