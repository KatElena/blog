<?php


class Model 
{
    //Ініціювати обєкт за даними масива: кожній властивості обєкта 
    //задати значення з відповідного елемента масива
    public function initObjectFromArray($array) 
    {
        foreach ($array as $key => $value) {
            $property = lcfirst(str_replace("_", "", ucwords($key, "_")));
            $this->$property = $value;
        }
    }
}
