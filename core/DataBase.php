<?php


class DataBase 
{
    public static $connection;
    
    public static function connect()
    {
        $dsn = "mysql:host=" . DB_HOSTNAME . ";dbname=" . DB_DATABASE . ";"
                . "port=" . DB_PORT . ";charset=utf8";
        self::$connection = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
        self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        //echo "Connection successful";
    }
}
