<?php

return [
    ""                  => "home/index",
    "page404"           => "home/page404",
    
    "post/([0-9]+)/([0-9]+)" => "post/view/$1/$2",
    "posts"             => "post/list",
    "post/add"          => "post/add",
    
    "category/([0-9]+)" => "category/view/$1",
    "categories"        => "category/list",
    
    "login"             => "user/login",
    "logout"            => "user/logout",
    "user/([0-9]+)"     => "user/view/$1",
    "users"             => "user/list",
    "register"          => "user/register",
    "user/submit"       => "user/submit",
];
