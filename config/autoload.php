<?php

spl_autoload_register(function($className) {
    $dirs = ["core", "models", "controllers"];
    foreach ($dirs as $dir) {
        if (is_file(ROOT . "/" . $dir . "/" . $className . ".php")) {
            include(ROOT . "/" . $dir . "/" . $className . ".php");
            break;
        }
    }
});
