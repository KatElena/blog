<?php

class CategoryController extends Controller
{
    
    public function actionList($parameters = [])
    {
        $this->title = "Список категорій";
        $categories = Category::getAllCategories();
        $params["categories"] = $categories;
        $this->render("list", $params);
    }
    
    public function actionView($parameters = [])
    {
        $categoryId = $parameters[0];
        $category = new Category($categoryId);
        $params["category"] = $category;
        //$params["categoryId"] = $categoryId;
        $this->title = $category->name;
        $this->render("view", $params);
    }
}
