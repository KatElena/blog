<?php

class HomeController extends Controller
{
    public function actionIndex($parameters = [])
    {
        $this->title = "Головна сторінка";
        $this->render("home");
    }
    
    public function actionPage404($parameters = [])
    {
        $this->title = "Сторінка 404";
        $this->render("page404");
    }
}
