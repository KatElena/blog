<?php

class UserController extends Controller
{
    public function actionList($parameters = [])
    {
        $this->title = "Список статей";
        $users = User::getAllUsers();
        $params["users"] = $users;
        $this->render("list", $params);
    }
    
    public function actionView($parameters = [])
    {
        $userId = $parameters[0];
        //Ініціюємо обєкт користувача з усіма його властивостями
        $user = new User($userId);
        //Передаємо даний обєкт в шаблон
        $params["user"] = $user;
        //$params["userId"] = $userId;
        $this->title = $user->firstName . " " . $user->lastName;
        $this->render("view", $params);
    }
    
    public function actionRegister($parameters = [])
    {
        $this->title = "Реєстрація користувача";
        $this->render("register");
    }

    public function actionLogin($parameters = [])
    {
        $this->title = "Авторизація користувача";
        $this->render("login");
    }
    
    public function actionLogout($parameters = [])
    {
        session_destroy();
        header("Location: /");
    }
    
    public function actionSubmit($parameters = [])
    {
        if (isset($_POST["action"]) && $_POST["action"] == "register")
        {
            $user = new User();
            $user->userId = $user->register();
            //Якщо реєстрація успішна, то переходимо автоматично на сторінку користувача
            if ($user->userId > 0) {
                header("Location: /user/" . $user->userId);
            }
        } elseif (isset($_POST["action"]) && $_POST["action"] == "login") {
            $userArray = User::checkUser();
            if (!empty($userArray)) {
                $_SESSION['user'] = $userArray;
                header("location: /user/" . $userArray['user_id']);
            }
        }
    }
}
