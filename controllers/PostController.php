<?php

class PostController extends Controller
{
    public function actionList($parameters = [])
    {
        $this->title = "Список статей";
        $posts = Post::getAllPosts();
        $params["posts"] = $posts;
        $this->render("list", $params);
    }
    
    public function actionView($parameters = [])
    {
        $postId = $parameters[0];
        $categoryId = $parameters[1];
        $post = new Post($postId);
        $category = new Category($categoryId);
        
        $params["post"] = $post;
        $params["category"] = $category;
        //$params["postId"] = $postId;
        $this->title = $post->title;
        $this->render("view", $params);
    }
    
    public function actionAdd($parameters = [])
    {
        if (isset($this->user) && $this->user->isLogin) {
            if (isset($_POST['action']) && $_POST['action'] == 'add_post') {
                $post = new Post();
                $post->authorId = $this->user->userId;
                $post->postId = $post->create();
                if ($post->postId > 0) {
                    header("Location: /post/" . $post->postId);
                }
                
            }
            $this->title = "Додавання публікації";
            $params["categories"] = $this->categories;
            $this->render("add", $params);
        } else {
            header("Location: /login");
        }
    }
}
